package edu.siu.cs425.hadoopmotifdetector;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MotifReducer extends Reducer<Text, MotifMapperWritable, Text, IntWritable>{
	public List<String> bestMotifs = new LinkedList<>();
	public List<Integer> bestDistances = new LinkedList<>();
	
	
	@Override
	public void reduce(Text key, Iterable<MotifMapperWritable> values, Context output)
			throws IOException, InterruptedException{
		int totalDistance = 0;
		
		for (MotifMapperWritable value: values) {
			totalDistance += value.getDistance().get();
		}
		bestMotifs.add(key.toString());
		bestDistances.add(new Integer(totalDistance));
	}
	
	@Override
	public void cleanup(Context output) throws IOException, InterruptedException {
		
		List<String> tempMotifs = new LinkedList<String>();
		
		Integer bestDistance = bestDistances.get(0); //Start with first motif as best
		tempMotifs.add(bestMotifs.get(0));
		
		for(int i=1; i < bestMotifs.size(); i++) {
			int compare = bestDistance.compareTo(bestDistances.get(i));
			
			if(compare == 0) {
				tempMotifs.add(bestMotifs.get(i));
			}
			else if(compare > 0) {
				bestDistance = bestDistances.get(i); //set as new best
				tempMotifs.clear(); //clear array of old best
				tempMotifs.add(bestMotifs.get(i)); // add new best motif
			}
		}
		
		for(int i = 0; i < tempMotifs.size(); i++){
			//write best motif to output
			
			output.write(new Text(tempMotifs.get(i)), new IntWritable(bestDistance.intValue()));
		}
		
	}
}
