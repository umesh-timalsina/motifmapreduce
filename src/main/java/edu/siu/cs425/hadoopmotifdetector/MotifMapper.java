package edu.siu.cs425.hadoopmotifdetector;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class MotifMapper extends Mapper<Object, Text, Text, MotifMapperWritable> {
	private final static MotifMapperWritable one = new MotifMapperWritable();
	private final static Logger logger = LoggerFactory.getLogger(MotifMapper.class);
	
	@Override
	public void map(Object key, Text value, Context output) throws IOException, InterruptedException{
		int TARGET_LENGTH = 8;
		TargetMotifGenerator.generateTargetMotifs(TARGET_LENGTH);
		for (String targetMotif: TargetMotifGenerator.allPossibleSequences) {
			String bestMatch = value.toString().substring(0, TARGET_LENGTH);
			int distance = 0;
			int bestDistance = TARGET_LENGTH + 1; // Why not inf?
			
			int currIndex = 0;
			int tmpCurrIndex = 0;
			for(int startIndex=0; startIndex < (value.toString().length() - TARGET_LENGTH); startIndex++) {
				currIndex = 0;
				distance = 0;
				for(char single: targetMotif.toCharArray()) {
					if (single != value.charAt(startIndex + currIndex++)) {
						distance++;
					}
				}
				
				if(distance < bestDistance) {
					bestDistance = distance;
					tmpCurrIndex = startIndex;
				}
			}
			one.setDistance(new IntWritable(bestDistance));
			one.setIndex(new IntWritable(tmpCurrIndex));
			one.setSequenceID(new IntWritable(Integer.parseInt(key.toString())));
//			logger.info("Best Distance: {} , Target Motif: {}, SequenceId: {}, StartIndex: {}", bestDistance, targetMotif, key.toString(), tmpCurrIndex);
			output.write(new Text(targetMotif), one);
		}
		
	}
}
