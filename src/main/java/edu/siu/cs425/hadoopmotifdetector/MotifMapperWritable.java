package edu.siu.cs425.hadoopmotifdetector;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.WritableComparable;

public class MotifMapperWritable implements WritableComparable<MotifMapperWritable>{
	private IntWritable distance, index, sequenceID;

	public MotifMapperWritable() {
		super();
		this.distance = new IntWritable();
		this.index = new IntWritable();
		this.sequenceID = new IntWritable();
	}
	
	public MotifMapperWritable(IntWritable distance, IntWritable index, IntWritable sequenceID) {
		super();
		this.distance = distance;
		this.index = index;
		this.sequenceID = sequenceID;
	}

	public IntWritable getDistance() {
		return distance;
	}

	public void setDistance(IntWritable distance) {
		this.distance = distance;
	}

	public IntWritable getIndex() {
		return index;
	}

	public void setIndex(IntWritable index) {
		this.index = index;
	}

	public IntWritable getSequenceID() {
		return sequenceID;
	}

	public void setSequenceID(IntWritable sequenceID) {
		this.sequenceID = sequenceID;
	}

	@Override
	public void readFields(DataInput input) throws IOException {
		distance.readFields(input);
		index.readFields(input);
		sequenceID.readFields(input);
	}

	@Override
	public void write(DataOutput arg0) throws IOException {
		distance.write(arg0);
		index.write(arg0);
		sequenceID.write(arg0);
	}

	@Override
	public int compareTo(MotifMapperWritable o) {
		// TODO Auto-generated method stub
		return distance.compareTo(o.getDistance());
	}
	
	

}
